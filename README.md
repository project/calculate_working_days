Calculate Working Days
----------------------------

Calculate Working Days is a module that allows you to calculate working days 
between 2 dates or the final date given an initial date 
and the number of working days.
It also can calculate which days in a month are free or working days.



CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


REQUIREMENTS
------------
  This module requires datepicker library.


INSTALLATION
------------

 *Install as you would normally install a contributed Drupal module. Visit
  [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules) for further information.


CONFIGURATION
-------------

To use:

    1. Navigate to Administration > Extend > And Search 
    for Calculate Working Days Module
    2. Enable the working days Module.
    3. Go to URL "/admin/config/regional/calculate-working-days"
    4. Do the configuration for free days of the week.
    5. Enter the day month and special occasions.


MAINTAINERS
-----------

- Pankaj kumar  - (https://www.drupal.org/u/pankaj_lnweb)
- Shikha Dawar  - (https://www.drupal.org/u/shikha_lnweb)
- Vivek kumar  - (https://www.drupal.org/u/vivek_lnwebworks)
