/**
 * @file
 * User interface javascript.
 * 
 * Converts an ugly textarea into a nice form.
 *
 */

(function (jQuery) {
  var rebuildDateInfo = function(){
    var info = "";
    var infoArray = [];
    var j = 0;
    jQuery('.free-days-info input').each(function(i, val){
      var type = jQuery(this).attr('type');
      if(type === "text"){
        infoArray[j] = "" + jQuery(this).val();
       }
      else if(type === "checkbox"){
       if (!jQuery(this).prop('checked')) {
        
      infoArray[j] += "-" + new Date().getFullYear();
    }
        ++j;
      }
    });
    
    info = infoArray.join("\n");
    
    return info;
  };


    var rebuildDateOcassionInfo = function(){
    var info = "";
    var infoArray = [];
    var j = 0;
    jQuery('.free-ocassion-days-info input').each(function(i, val){
      var type = jQuery(this).attr('type');
      if(type === "text"){
        infoArray[j] = "" + jQuery(this).val();
        
      }else if(type === "checkbox"){
        if (!jQuery(this).prop('checked')) {

          infoArray[j] +="";
            }
     
        ++j;
      }
    });
    info = infoArray.join("\n");
    return info;
  };
  
  
  
  function freeDayTable(dates, labels, addLabel) {
    var output = "";

    output += '<table id="ui-free-days">';
    output += '<tr>';
    output += '<th>' + labels.days + '</th>';
    output += '<th>' + labels.check + '</th>';
    output += '<th>' + labels.operations + '</th>';
    output +=  "</tr>";
    for(i in dates){
      var numbers = dates[i].split("-");
      var checked = " checked=checked";
      if(numbers.length === 3){
        checked = "";
      }
      if(numbers[0]==false){
        var currentDate = '';
      }else{
        var currentDate = numbers[0] + '-' + numbers[1];
      }
        output += freeDayRow(currentDate, i, checked, labels.delete);
    }
    output += '</table>';
    
    output += '<a href="#" id="add-free-day">' + addLabel + '</a>';

    return jQuery(output);
  };
  
    function freeOcassionTable(dates, labels) {
    var output = "";

    output += '<table id="ui-free-days_ocassion">';
    output += '<tr>';
    output += '<th>Special Ocassions</th>';
    output += '<th>Every Year</th>';
    output += '<th>Operations</th>';
    output +=  "</tr>";
    for(i in dates){
      var numbers = dates[i];
      var checked = "checked=checked";
   

      if(numbers.replace ( /[^\d.]/g, '' ).length === 4){
        checked = "";
      }
      
      var currentOcassion = numbers;
      output += freeOcassionRow( currentOcassion, i, checked, labels.delete);
    }
    output += '</table>';
    
    output += '<a href="#" id="add-free-ocassion-day">Add another ocassion</a>';

    return jQuery(output);
  };

  function freeOcassionRow(currentOcassion, index,checked, sDelete = 'Delete') {
    var output = "";
    output += '<tr class="free-ocassion-days-info">';
    output += '<td>';
    output += '<input name="freeocassion-'+index+'" type="text" class="ocassion" value="'+ currentOcassion + '" style="border: 1px solid lightgray;padding: 5px;">';
    output +=  "</td><td>";
    output += '<input type="checkbox" name="check-'+index+'" '+ checked +'/>';
    output +=  "</td>"; 
    output +=  "</td><td>";
    output += '<a href="#" class="delete">' + sDelete + '</a>';
    output +=  "</td>";
    output +=  "</tr>";
    
    return output;
  };



  
  function freeDayRow(currentDate, index, checked, sDelete) {
    var output = "";
    output += '<tr class="free-days-info">';
    output += '<td>';
    output += '<input name="date-'+index+'" type="text" class="dates" value="'+ currentDate + '" style="border: 1px solid lightgray;padding: 5px;">';
    output +=  "</td><td>";
    output += '<input type="checkbox" name="check-'+index+'" '+ checked +'/>';
    output +=  "</td>"; 
    output +=  "</td><td>";
    output += '<a href="#" class="delete">' + sDelete + '</a>';
    output +=  "</td>";
    output +=  "</tr>";
   
    return output;
  };
  
  
  
  Drupal.behaviors.freeDayBehavior = {
    attach: function (context, settings) {

        jQuery('#free-days', context).once('myCustomBehavior').each(function () {


        var dates = jQuery('#edit-days').val().split("\n");
        var labels = {
          "days"       : 'Day-Month',
          "check"      : 'Every-year',
          "operations" : 'OPERATIONS',
          "delete"     : 'Delete'
        };

        var jQuerytable = freeDayTable(dates, labels, 'Add another holiday');
        
        jQuery(this).append(jQuerytable);
        
        
        jQuery('.delete').click(function(){
          jQuery(this).parent().parent().remove();
        
        });
        
        if(jQuery('div.messages.error').length === 0){
          jQuery('.form-item-days').hide();
        }
        
        jQuery("#edit-submit").click(function(){
          var info = rebuildDateInfo();
          jQuery('#edit-days').val(info);
        });
        
        jQuery( ".dates" ).datepicker({ dateFormat: "dd-mm" });
        
        /* Event: Adds a new line */
        jQuery('#add-free-day').click(function(){
          var rows = jQuery('#ui-free-days').find('tr').length;
          var today = new Date();
          var day = today.getDate() + "";
          var month = (today.getMonth() + 1) + "";
          
          if(month.length < 2){
            month = "0" + month;
          }
          if(day.length < 2){
            day = "0" + day;
          }
          
          var currentDate = day + "-" + month;
          
          var output = "";
          var sDelete = 'Delete';
          output += freeDayRow(currentDate, rows, "", sDelete);
          jQuery('#ui-free-days').append(output);
          
          jQuery('#ui-free-days .dates:last').datepicker({ dateFormat: "dd-mm" });
          
          jQuery('.delete').click(function(){
            jQuery(this).parent().parent().remove();
          
            return false;
          });
          
          var info = rebuildDateInfo();
          jQuery('#edit-days').val(info);
          
          jQuery(".free-days-info .dates, .free-days-info input[type=checkbox]").change(function(){
            var info = rebuildDateInfo();
            jQuery('#edit-days').val(info);
          });
          
          return false;
        });
        
        /* Event: refreshes textarea data if table changes */
        jQuery(".free-days-info .dates, .free-days-info input[type=checkbox]").change(function(){
          var info = rebuildDateInfo();
          jQuery('#edit-days').val(info);
        });
      });

    }
  };


    Drupal.behaviors.freeocassion = {
    attach: function (context, settings) {
      jQuery('#free-ocassion',context).once('myCustomBehavior1').each(function () {
        var dates = jQuery('#edit-ocassion').val().split("\n");
        var labels = {
          "days"       : "Special Ocassions",
        };
  
        var jQuerytable = freeOcassionTable(dates, labels);
        
        jQuery(this).append(jQuerytable);
        
        
        jQuery('.delete').click(function(){
          jQuery(this).parent().parent().remove();
       
        });
        
       if(jQuery('div.messages.error').length === 0){
          jQuery('.form-item-ocassion').hide();
        }
        
        jQuery("#edit-submit").click(function(){
          var info = rebuildDateOcassionInfo();
          jQuery('#edit-ocassion').val(info);

        });
        
        // jQuery( ".dates" ).datepicker({ dateFormat: "dd-mm" });
        
        /* Event: Adds a new line */
        jQuery('#add-free-ocassion-day').click(function(){
          var rows = jQuery('#ui-free-days_ocassion').find('tr').length;        
          var currentDate = " ";
          
          var output = "";
          var sDelete = "Delete";
          output += freeOcassionRow(currentDate, rows, "", sDelete);
          jQuery('#ui-free-days_ocassion').append(output);
          
          jQuery('#ui-free-days .dates:last').datepicker({ dateFormat: "dd-mm" });
          
          jQuery('.delete').click(function(){
            jQuery(this).parent().parent().remove();
         
            return false;
          });
          
          var info = rebuildDateOcassionInfo();
          jQuery('#edit-ocassion').val(info);
          
          jQuery(".free-ocassion-days-info .ocassion, .free-ocassion-days-info input[type=checkbox]").change(function(){
            var info = rebuildDateOcassionInfo();
            jQuery('#edit-ocassion').val(info);
          });
        
          
          return false;
        });
       
        /* Event: refreshes textarea data if table changes */
       jQuery(".free-ocassion-days-info .ocassion, .free-ocassion-days-info input[type=checkbox]").change(function(){
          var info = rebuildDateOcassionInfo();
          jQuery('#edit-ocassion').val(info);
        }); 
      });
    }
  };

})(jQuery);