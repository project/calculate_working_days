<?php

namespace Drupal\calculate_working_days;

/**
 * @file
 * The theme system, which controls the output of Drupal.
 *
 * The theme system allows for nearly all output of the Drupal system to be
 * customized by user themes.
 */


/**
 * This class calculates working days within data ranges.
 */
class CalculateWorkingDays {
  /**
   * Holidays fixed for each year. No exception.
   *
   * @var array
   */
  protected $freeDaysAll;

  /**
   * Holidays fixed for each year. freeDaysOcassionAll.
   *
   * @var array
   */
  protected $freeDaysOcassionAll;


  /**
   * Variable holiday. Different from one year to the other.
   *
   * @var array
   */
  protected $freeDaysException;

  /**
   * Variable holiday. Different from one year to the other.
   *
   * @var array
   */
  protected $freeDaysOcassionException;

  /**
   * Days of the week considered free. No exception.
   *
   * From 1 to 7. 1 is monday and 7 is sunday.
   *
   * @var array
   */
  protected $freeWeekDays;

  /**
   * Constructor .
   */
  public function __construct() {
    $this->freeDaysAll               = [];
    $this->freeDaysOcassionAll       = [];
    $this->freeDaysOcassionException = [];
    $this->freeDaysException         = [];
    $this->freeWeekDays              = [];
  }

  /**
   * Sets the days of the week in which there is no work.
   *
   * @param array $weekDays
   *   - Each value must be in the range [1, 7]. 1 for monday, 7 for sunday.
   *
   * @return \CalculateWorkingDays
   *   working days
   *
   * @throws InvalidArgumentException
   */
  public function setFreeWeekDays(array $weekDays) {
    foreach (array_filter($weekDays) as $weekDay) {
      if (!is_numeric($weekDay) || $weekDay > 7 || $weekDay < 1) {
        throw new InvalidArgumentException("Week day must be a number in [1-7] range");
      }
    }

    $this->freeWeekDays = $weekDays;

    return $this;
  }

  /**
   * Gets common FreeDays defined.
   *
   * @return mixed
   *   if the year is FALSE then is a holiday valid for each year. If is
   *    numeric then is just valid for that number   *
   */
  public function getFreeDaysAll() {
    return $this->freeDaysAll;
  }

  /**
   * Gets common Free ocassion defined.
   *
   * @return mixed
   *   if the year is FALSE then is a holiday valid for each year. If is
   *    numeric then is just valid for that number   *
   */
  public function getFreeOcassionDaysAll() {
    return $this->freeDaysOcassionAll;

  }

  /**
   * Adds a holiday to the list.
   *
   * @param int $day
   *   Between 1 and 31.
   * @param int $month
   *   Between 1 and 12.
   * @param mixed $year
   *   if the year is FALSE then is a holiday valid for each year. If is
   *    numeric then is just valid for that number.
   *
   * @return \CalculateWorkingDays
   *   working days
   *
   * @throws InvalidArgumentException
   */
  public function addFreeDay($day, $month, $year = FALSE) {
    if (!is_int($day) || !is_int($month)) {
      throw new InvalidArgumentException("Day and month should be integer");
    }
    if ($year !== FALSE && !is_int($year)) {
      throw new InvalidArgumentException("Year should be integer");
    }
    if ($year !== FALSE) {
      $this->freeDaysException[$year][$month][$day] = TRUE;
    }
    else {
      $this->freeDaysAll[$month][$day] = TRUE;
    }

    return $this;
  }

  /**
   * Get Free days occassion exception .
   */
  public function addFreeOcassionDay($day, $month, $year) {
    /*watchdog('year',$year);
    watchdog('month',$month);
    watchdog('day',$day);
     */$this->freeDaysOcassionException[$year][$month][$day] = TRUE;
    return $this;
  }

  /**
   * Add Free days occassion.
   */
  public function addFreeOcassionDays($day, $month, $year) {
    /*watchdog('yearall',$year);
    watchdog('monthall',$month);
    watchdog('dayall',$day);
     */$this->freeDaysOcassionAll[$year][$month][$day] = TRUE;
    return $this;
  }

  /**
   * Calculate working days between 2 dates (both included)
   *
   * @param int $startDate
   *   The timestamp representing the start date.
   * @param int $endDate
   *   The timestamp representing the end date.
   *
   * @return int
   *   How many working days
   */
  public function calculateWorkingDays($startDate, $endDate) {
    $workingDays = 0;

    $currentDate = $startDate;
    $secondsInDay = (60 * 60 * 24);

    while ($currentDate < $endDate) {

      if (in_array(date("N", $currentDate), $this->freeWeekDays)) {
        $currentDate = $currentDate + $secondsInDay;
        continue;
      }

      $currentDay   = (int) date("d", $currentDate);
      $currentMonth = (int) date("m", $currentDate);
      $currentYear  = (int) date("Y", $currentDate);

      if (isset($this->freeDaysAll[$currentMonth][$currentDay])) {
        $currentDate = $currentDate + $secondsInDay;
        continue;
      }

      if (isset($this->freeDaysOcassionAll[$currentYear][$currentMonth][$currentDay])) {
        $currentDate = $currentDate + $secondsInDay;
        continue;
      }

      if (isset($this->freeDaysException[$currentYear][$currentMonth][$currentDay])) {
        $currentDate = $currentDate + $secondsInDay;
        continue;
      }

      if (isset($this->freeDaysOcassionException[$currentYear][$currentMonth][$currentDay])) {
        $currentDate = $currentDate + $secondsInDay;
        continue;
      }

      $currentDate = $currentDate + $secondsInDay;
      ++$workingDays;
    }

    return $workingDays;
  }

  /**
   * Given a start date and a number of work days, it returns the final date.
   *
   * @param int $startDate
   *   The timestamp representing the start date.
   * @param int $workingDays
   *   Days for work.
   *
   * @return timestamp
   *   Get a timestamp representing a specific date
   */
  public function calculateEndDate($startDate, $workingDays) {
    $daysToConsume = $workingDays;

    $currentDate = $startDate;
    $minutesDay = (60 * 60 * 24);

    while ($daysToConsume > 0) {

      $currentDate = $currentDate + $minutesDay;

      $isWeekend = in_array(date("N", $currentDate), $this->freeWeekDays);

      $currentDay   = (int) date("d", $currentDate);
      $currentMonth = (int) date("m", $currentDate);
      $currentYear  = (int) date("Y", $currentDate);

      $thisIsFree =
          isset($this->freeDaysAll[$currentMonth][$currentDay]) ||
          isset($this->freeDaysException[$currentYear][$currentMonth][$currentDay]);

      $thisIsFree2 = isset($this->freeDaysOcassionAll[$currentYear][$currentMonth][$currentDay]) || isset($this->freeDaysOcassionException[$currentYear][$currentMonth][$currentDay]);

      if ($thisIsFree || $isWeekend || $thisIsFree2) {
        continue;
      }
      else {
        --$daysToConsume;
      }
    }

    return $currentDate;
  }

  /**
   * Gives all free days in a month for a specific year.
   *
   * @param int $month
   *   The month (numeric representation).
   * @param int $year
   *   The year (numeric representation).
   *
   * @return array
   *   - The array values are the free days for that month and year,
   */
  public function getMonthFreeDays($month, $year) {
    $freeDays = [];
    $currentDate = mktime(0, 0, 0, $month, 1, $year);
    $currentMonth = $month;

    $secondsDay = (60 * 60 * 24);

    $i = 1;
    while ($currentMonth === $month) {
      $isWeekend = in_array(date("N", $currentDate), $this->freeWeekDays);

      $currentDay   = (int) date("d", $currentDate);
      $currentMonth = (int) date("m", $currentDate);
      $currentYear  = (int) date("Y", $currentDate);

      $thisIsFree =
        isset($this->freeDaysAll[$currentMonth][$currentDay]) ||
        isset($this->freeDaysException[$currentYear][$currentMonth][$currentDay]);

      $thisIsFree2 = isset($this->freeDaysOcassionAll[$currentYear][$currentMonth][$currentDay]) || isset($this->freeDaysOcassionException[$currentYear][$currentMonth][$currentDay]);

      if ($thisIsFree || $isWeekend || $thisIsFree2) {
        $freeDays[] = $i;
      }

      $currentDate += $secondsDay;
      $currentMonth = (int) date("m", $currentDate);

      if ($i++ > 31) {
        break;
      }
    }

    return $freeDays;
  }

  /**
   * Gives all working days in a month for a specific year.
   *
   * @param int $month
   *   The month (numeric representation).
   * @param int $year
   *   The year (numeric representation).
   *
   * @return array
   *   - The array values are the working days for that month and year,
   */
  public function getMonthWorkingDays($month, $year) {
    $workingDays = [];
    $currentDate = mktime(0, 0, 0, $month, 1, $year);
    $currentMonth = $month;

    $secondsDay = (60 * 60 * 24);

    $i = 1;
    while ($currentMonth === $month) {
      $isWeekend = in_array(date("N", $currentDate), $this->freeWeekDays);

      $currentDay   = (int) date("d", $currentDate);
      $currentMonth = (int) date("m", $currentDate);
      $currentYear  = (int) date("Y", $currentDate);

      $thisIsFree =
        isset($this->freeDaysAll[$currentMonth][$currentDay]) ||
        isset($this->freeDaysException[$currentYear][$currentMonth][$currentDay]);

      $thisIsFree2 = isset($this->freeDaysOcassionAll[$currentYear][$currentMonth][$currentDay]) || isset($this->freeDaysOcassionException[$currentYear][$currentMonth][$currentDay]);

      if (!$thisIsFree && !$isWeekend && !$thisIsFree2) {
        $workingDays[] = $i;
      }

      $currentDate += $secondsDay;
      $currentMonth = (int) date("m", $currentDate);

      if ($i++ > 31) {
        break;
      }
    }

    return $workingDays;
  }

}
