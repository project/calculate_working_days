<?php

namespace Drupal\calculate_working_days\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure calculate_working_days settings for this site.
 */
class CalculateWorkDaysForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'calculate_working_days.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'calculate_working_days_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'calculate_working_days.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('calculate_working_days.settings');

    $form['#attached']['library'][] = 'calculate_working_days/calculate_working_days';
    $form['#attached']['library'][] = 'system/ui.datepicker';

    $weekdays = [
      1 => $this->t("Monday"),
      2 => $this->t("Tuesday"),
      3 => $this->t("Wednesday"),
      4 => $this->t("Thursday"),
      5 => $this->t("Friday"),
      6 => $this->t("Saturday"),
      7 => $this->t("Sunday"),
    ];
    $form['weekdays'] = [
      '#type'    => 'checkboxes',
      '#options' => $weekdays,
      '#default_value' => ($config->get('weekdays') === NULL) ? [] : array_filter($config->get('weekdays'), function ($a) {
        return ($a !== 0);
      }),
      '#title'   => $this->t('Free days of every week'),
    ];

    $form['days'] = [
      '#title' => $this->t('Set holidays'),
      '#type'  => 'textarea',
      '#default_value' => $config->get('days'),
      '#description' => $this->t('Each per line. 03-12 format for every 3th of December. 03-12-2014 just for that year.'),
      '#prefix' => '<div id="days" >',
      '#suffix' => '</div>',

    ];
    $form['ocassion'] = [
      '#title' => $this->t('Set Ocassion'),
      '#type'  => 'textarea',
      '#default_value' => $config->get('ocassion'),
      '#description' => $this->t('Add ocassion like  e.g.last monday of april.'),
      '#prefix' => '<div id="ocassion" >',
      '#suffix' => '</div>',
    ];

    $form['ui'] = [
      '#type'   => 'markup',
      '#markup' => '<div id="free-days"></div>',
    ];

    $form['ui_sp'] = [
      '#type'   => 'markup',
      '#markup' => '<div id="free-ocassion"></div>',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->config('calculate_working_days.settings')
      // Set the submitted configuration setting.
      ->set('weekdays', $form_state->getValue('weekdays'))
      ->set('days', $form_state->getValue('days'))
      ->set('ocassion', $form_state->getValue('ocassion'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
